style:
	black .
	isort .

build-amd64:
	docker buildx build --push --platform linux/amd64 --tag "$(IMAGE):$(TAG)-amd64" .

build-arm64:
	docker buildx build --push --platform linux/arm64 --tag "$(IMAGE):$(TAG)-arm64" .

build-manifest:
	docker manifest create $(IMAGE):$(TAG) $(IMAGE):$(TAG)-amd64 $(IMAGE):$(TAG)-arm64
	docker manifest inspect $(IMAGE):$(TAG)
	docker manifest push $(IMAGE):$(TAG)

run-selenium-amd:
	docker run -d -p 4444:4444 selenium/standalone-chrome:latest

run-selenium-arm:
	docker run -d -p 4444:4444 seleniarm/standalone-chromium:latest