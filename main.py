import os
from locale import currency

from loguru import logger

from slack import SlackHelper
from vinted import VintedSearch

logger.add("file_{time}.log")

slack_token = os.getenv("SLACK_BOT_TOKEN")
sh = SlackHelper(slack_token)

session_cookie = sh.get_cookie()
alerts = sh.get_alerts()

vs = VintedSearch(session_cookie)

message = """
[
    {{
        "type": "section",
        "text": {{
            "type": "mrkdwn",
            "text": "*Titre :* {title}\n*Prix :* {price} {currency}"
        }},
        "accessory": {{
            "type": "button",
            "text": {{
                "type": "plain_text",
                "text": "Voir sur Vinted",
                "emoji": true
            }},
            "value": "vinted",
            "url": "{url}",
            "action_id": "button-action"
        }}
    }},
    {{
        "type": "image",
        "image_url": "{image_url}",
        "alt_text": "photo_item"
    }},
    {{
        "type": "context",
        "elements": [
            {{
                "type": "plain_text",
                "text": "item_id: {item_id}",
                "emoji": true
            }}
        ]
    }},
    {{
        "type": "divider"
    }}
]
"""

for alert in alerts:
    results = vs.search(
        parameters=alert["parameters"],
        last_item_id=alert["last_item_id"],
        max_results_number=alert["max_results_number"],
    )
    for result in reversed(results):
        sh.send_blocks(
            alert["channel"],
            message.format(
                title=result["title"],
                price=result["price"],
                currency=result["currency"],
                url=result["url"],
                item_id=result["id"],
                image_url=result["photo"]["url"],
            ),
        )

sh.send_message(sh.cookie_channel, vs.get_session_cookie())
