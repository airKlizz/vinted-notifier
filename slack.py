import html
import json
import os
import re
import sys
import urllib.parse
from typing import Dict, List

from loguru import logger
from slack_sdk import WebClient


class SlackHelper:

    cookie_channel = "C0473C8K7HP"
    alert_pattern = r"alert"
    item_id_pattern = r"item_id: ([^.]+)"

    def __init__(self, slack_token: str) -> None:
        self.client = WebClient(token=slack_token)
        logger.success("Slack client initialized")

    @logger.catch(
        onerror=lambda _: sys.exit(1),
        message="Error while getting the cookie from Slack",
    )
    def get_cookie(self) -> str:
        result = self.client.conversations_history(channel=self.cookie_channel, limit=1)
        return result["messages"][0]["text"]

    @logger.catch(
        onerror=lambda _: sys.exit(1), message="Error while getting alerts from Slack"
    )
    def get_alerts(self) -> List[Dict[str, str]]:
        result = self.client.conversations_list()
        alerts = []
        for channel in result["channels"]:
            if re.search(self.alert_pattern, channel["name"]):
                try:
                    result = self.client.conversations_history(
                        channel=channel["id"], limit=1, include_all_metadata=True
                    )
                    logger.info("Message:\n{msg}", msg=result)
                    text = result["messages"][0]["blocks"][-2]["elements"][0]["text"]
                    logger.info("Last text message: {text}", text=text)
                    last_item_id = re.findall(self.item_id_pattern, text)[0]
                    logger.info(
                        "Last item id found: {last_item_id}", last_item_id=last_item_id
                    )
                    max_results_number = 10
                except:
                    last_item_id = None
                    max_results_number = 1
                parameters = {}
                for key, value in urllib.parse.parse_qs(
                    html.unescape(channel["purpose"]["value"])
                ).items():
                    for v in value:
                        # convert parameters keys to be compatible with the api
                        # catalog[] -> catalog_ids
                        # size_id[] -> size_ids
                        if key[-2:] == "[]":
                            if key[-4:-2] == "id":
                                key = key[:-2] + "s"
                            else:
                                key = key[:-2] + "_ids"
                        if key in parameters.keys():
                            parameters[key] += "," + v
                        else:
                            parameters[key] = v
                alerts.append(
                    {
                        "channel": channel["id"],
                        "parameters": [(k, v) for k, v in parameters.items()],
                        "last_item_id": last_item_id,
                        "max_results_number": max_results_number,
                    }
                )
        logger.success("Alerts retrieved from Slack")
        return alerts

    @logger.catch(
        onerror=lambda _: sys.exit(1), message="Error while sending a message to Slack"
    )
    def send_message(self, channel: str, message: str):
        self.client.chat_postMessage(channel=channel, text=message)
        logger.success("Slack message sent")

    @logger.catch(
        onerror=lambda _: sys.exit(1), message="Error while sending a message to Slack"
    )
    def send_blocks(self, channel: str, blocks: str):
        self.client.chat_postMessage(channel=channel, blocks=blocks)
        logger.success("Slack message sent")
