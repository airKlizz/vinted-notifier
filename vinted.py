import http.client
import json
import os
import re
import sys
import time
import urllib.parse
from typing import Any, Dict, List, Mapping, Sequence, Tuple, Union

from loguru import logger
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class VintedSearch:

    host = "www.vinted.fr"
    url = "/api/v2/catalog/items"
    cookie = "_vinted_fr_session={session_cookie}"
    session_cookie_pattern = r"_vinted_fr_session=([^;]+)"

    def __init__(self, session_cookie: str) -> None:
        self.headers = {"cookie": self.cookie.format(session_cookie=session_cookie)}

    @logger.catch(
        onerror=lambda _: sys.exit(1),
        message="Error while getting session cookie from self header",
    )
    def get_session_cookie(self):
        return re.findall(self.session_cookie_pattern, self.headers["cookie"])[0]

    @logger.catch(
        onerror=lambda _: sys.exit(1),
        message="Unexpected error while searching Vinted API",
    )
    def search(
        self,
        parameters: Sequence[tuple],
        last_item_id: Union[str, None] = None,
        max_results_number: int = 5,
        delay: float = 0.5,
    ) -> List[Dict]:
        assert type(max_results_number) == int and max_results_number > 0
        logger.info(
            "Vinted search with last item id: {id} and max_results_number: {nb}",
            id=last_item_id,
            nb=max_results_number,
        )
        parameters.append(("time", int(time.time())))
        parameters.append(("page", 1))
        parameters.append(("order", "newest_first"))
        results = []
        while len(results) < max_results_number:
            conn = http.client.HTTPSConnection(self.host)
            logger.info(
                "GET: {url}", url=self.url + "?" + urllib.parse.urlencode(parameters)
            )
            conn.request(
                "GET",
                self.url + "?" + urllib.parse.urlencode(parameters),
                "",
                self.headers,
            )
            response = conn.getresponse()
            logger.info(
                "Response from Vinted API [{status}]",
                status=response.status,
            )
            if response.status == 401:
                # invalid token
                self.get_new_session_cookie()
                continue
            items = self.process_response(response)
            logger.info(
                "Items found:\n{items}",
                items=" - ".join([item["title"] for item in items]),
            )
            for item in items:
                logger.info(
                    "item id: {item_id} - last item id: {last_item_id}",
                    item_id=item["id"],
                    last_item_id=last_item_id,
                )
                if str(item["id"]) == str(last_item_id):
                    return results
                logger.info("Add item {item} to results", item=item["title"])
                results.append(item)
            for key, value in parameters:
                if key == "page":
                    value += 1
            time.sleep(delay)
        logger.success("Vinted API search done")
        return results[:max_results_number]

    @logger.catch(
        onerror=lambda _: sys.exit(1), message="Error while getting new session cookie"
    )
    def get_new_session_cookie(self):
        options = webdriver.FirefoxOptions()
        options.headless = True
        logger.info("Connect to remote selenium")
        driver = webdriver.Remote(
            command_executor=f'{os.getenv("SELENIUM_URL")}/wd/hub', options=options
        )
        logger.info("Connected to remote selenium")
        try:
            logger.info("Get vinted URL")
            driver.get("https://www.vinted.fr/catalog")
            logger.info("Click cookie button")
            WebDriverWait(driver, 20).until(
                EC.element_to_be_clickable((By.ID, "onetrust-accept-btn-handler"))
            ).click()
            cookie = driver.get_cookie("_vinted_fr_session")
            logger.info("Set cookie in header")
            self.headers = {
                "cookie": self.cookie.format(session_cookie=cookie["value"])
            }
        finally:
            logger.info("Close Firefox")
            driver.close()

    @logger.catch(
        onerror=lambda _: sys.exit(1), message="Error while processing the response"
    )
    def process_response(self, response: http.client.HTTPResponse) -> List:
        if response.status != 200:
            raise ValueError(
                f"Response status code {response.status} - Response:\n{response.read().decode('utf-8')}"
            )
        data = response.read()
        headers = response.getheaders()
        items = json.loads(data.decode("utf-8"))
        new_session_cookie = VintedSearch.extract_session_cookie(headers)
        self.headers = {"cookie": self.cookie.format(session_cookie=new_session_cookie)}
        return items["items"]

    @classmethod
    def extract_session_cookie(cls, headers: List[Tuple[str, str]]) -> str:
        for header_name, header_value in headers:
            if header_name == "Set-Cookie" and re.search(
                cls.session_cookie_pattern, header_value
            ):
                session_cookie = re.findall(cls.session_cookie_pattern, header_value)[0]
                return session_cookie
        raise ValueError("New session cookie not found in the headers.")
